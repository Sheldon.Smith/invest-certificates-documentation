# Certificates

## **Contents**
  - [OAuth 2.0 - JWT Signing](#oauth-2.0-jwt-signing)
    - [Identity Provider](#oauth-2.0-jwt-signing:identity-provider)
      - [Actions](#oauth-2.0-jwt-signing:identity-provider:actions)
      - [Non-Production](#oauth-2.0-jwt-signing:identity-provider:non-production)
      - [Production](#oauth-2.0-jwt-signing:identity-provider:production)
  - [OAuth 2.0 - Client Assertion](#oauth-2.0-client-assertion)
    - [Money Transfers Relationship](#oauth-2.0-client-assertion:money-transfers-relationship)
      - [Actions](#oauth-2.0-client-assertion:money-transfers-relationship:actions)
      - [Non-Production](#oauth-2.0-client-assertion:money-transfers-relationship:non-production)
      - [Production](#oauth-2.0-client-assertion:money-transfers-relationship:production)
  - [SSO - SAML Validation](#sso-saml-validation)
    - [Developer Api](#sso-saml-validation:developer-api)
      - [Actions](#sso-saml-validation:developer-api:actions)
      - [Non-Production](#sso-saml-validation:developer-api:non-production)
      - [Production](#sso-saml-validation:developer-api:production)
    - [TAPI](#sso-saml-validation:tapi)
      - [Actions](#sso-saml-validation:tapi:actions)
      - [Non-Production](#sso-saml-validation:tapi:non-production)
      - [Production](#sso-saml-validation:tapi:production)
    - [LIVE Service Provider](#sso-saml-validation:live-service-provider)
      - [Actions](#sso-saml-validation:live-service-provider:actions)
      - [Non-Production](#sso-saml-validation:live-service-provider:non-production)
      - [Production](#sso-saml-validation:live-service-provider:production)
    - [OLA Service Provider](#sso-saml-validation:ola-service-provider)
      - [Actions](#sso-saml-validation:ola-service-provider:actions)
      - [Non-Production](#sso-saml-validation:ola-service-provider:non-production)
      - [Production](#sso-saml-validation:ola-service-provider:production)
  - [SSO - SAML Signing](#sso-saml-signing)
    - [Developer Api](#sso-saml-signing:developer-api)
      - [Actions](#sso-saml-signing:developer-api:actions)
      - [Non-Production](#sso-saml-signing:developer-api:non-production)
      - [Production](#sso-saml-signing:developer-api:production)
    - [TAPI](#sso-saml-signing:tapi)
      - [Actions](#sso-saml-signing:tapi:actions)
      - [Non-Production](#sso-saml-signing:tapi:non-production)
      - [Production](#sso-saml-signing:tapi:production)
    - [LIVE Service Provider](#sso-saml-signing:live-service-provider)
      - [Actions](#sso-saml-signing:live-service-provider:actions)
      - [Non-Production](#sso-saml-signing:live-service-provider:non-production)
      - [Production](#sso-saml-signing:live-service-provider:production)
    - [OLA Service Provider](#sso-saml-signing:ola-service-provider)
      - [Actions](#sso-saml-signing:ola-service-provider:actions)
      - [Non-Production](#sso-saml-signing:ola-service-provider:non-production)
      - [Production](#sso-saml-signing:ola-service-provider:production)
  - [Scheduled Jobs](#scheduled-jobs)
    - [Hangfire](#scheduled-jobs:hangfire)
      - [Actions](#scheduled-jobs:hangfire:actions)
      - [Development](#scheduled-jobs:hangfire:development)
      - [QA](#scheduled-jobs:hangfire:qa)
      - [Production](#scheduled-jobs:hangfire:production)
  - [Vendors](#vendors)
    - [Nasdaq Inc](#vendors:nasdaq-inc)
      - [Nasdaq Fund Network Feed](#vendors:nasdaq-inc:nasdaq-fund-network-feed)

<a id="oauth-2.0-jwt-signing"></a>
## **OAuth 2.0 - JWT Signing**

All certificates under this category are Invest-issued certificates responsible for signing JWTs (Json Web Tokens) required in modern backend-to-backend communication. JWTs and backend authorization policy enforcements enable fine-granularity access control for applications.

All certificates under this category typically expire at approximately the same datetime hence they are to be renewed at approximately the same datetime.<br/>

<a id="oauth-2.0-jwt-signing:identity-provider"></a>
### **Identity Provider**

Description: The identity provider is a publicly-facing service based on <a href="http://docs.identityserver.io/en/latest/">IdentityServer4</a> that supports OIDC and OAuth 2.0.<br/>

Technical Owners:
  * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
  * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>

Required: Yes<br/>
Renewal:  Yes<br/>
Obseletion: N/A

<a id="oauth-2.0-jwt-signing:identity-provider:actions"></a>
Actions:
  1. (Invest; DevOps) Import certificate into the Octopus certificate store
  2. (Invest; DevOps,Developers) Create a new release of the current production artifact and deploy. Annotate the release to inform of the certificate change (ex. suffix: -signing-certificate)<br/><br/>

Venafi:<br/>
  * Users/Groups to Notify:
    * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
    * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>
    * `NAO\LZ6R96` - <a href="mailto:ramu.bandaru@invest.ally.com">ramu.bandaru@invest.ally.com</a>
    * `NAO\Invest DevOps` - <a href="mailto:InvestDevOps@invest.ally.com">InvestDevOps@invest.ally.com</a>
    * `NAO\Invest SRE Engineering` - <a href="mailto:SREEngineers@invest.ally.com">SREEngineers@invest.ally.com</a><br/><br/>

<a id="oauth-2.0-jwt-signing:identity-provider:non-production"></a>
**Non-Production**
<ul style="list-style: none; margin-top: -8px; padding-left: 20px;">
  <li>Common Name: <strong>invest.int.ally.com</strong></li>
  <li>Has Private Key: Yes</li>
  <li>Thumbprint: <strong>EB85412B06F7AB6C7787044373180ED66CB232C1</strong></li>
  <li>Expiration: <strong>Friday, April 19, 2024 2:58 PM -04:00</strong></li>
  <li>Venafi Note:<br/><br/>
    <code>
      Non-Production - This certificate supports the identity provider. Renewal requires 1. an Octopus certificate store import and 2. installation on servers via Octopus deployments (project: allyinvest-idp) with updated variables. Invest Sustain manages this cert. For more details, consult <a href="https://confluence.int.ally.com/display/AIS/Certificates#Certificates-IdentityProvider">https://confluence.int.ally.com/display/AIS/Certificates#Certificates-IdentityProvider</a>
    </code>
  </li>
</ul>
<br/><br/>

<a id="oauth-2.0-jwt-signing:identity-provider:production"></a>
**Production**
<ul style="list-style: none; margin-top: -8px; padding-left: 20px;">
  <li>Common Name: <strong>invest.ally.com</strong></li>
  <li>Has Private Key: Yes</li>
  <li>Thumbprint: <strong>368E15F58EA5216842BF8F19DF489E3B72CBBAE2</strong>
  <li>Expiration: <strong>Sunday, April 21, 2024 12:44 PM -04:00</strong>
  <li>Venafi Note:<br/><br/>
    <code>Production - This certificate supports the identity provider. Renewal requires 1. an Octopus certificate store import and 2. installation on servers via Octopus deployments (project: allyinvest-idp) with updated variables. Invest Sustain manages this cert. For more details, consult <a href="https://confluence.int.ally.com/display/AIS/Certificates#Certificates-IdentityProvider">https://confluence.int.ally.com/display/AIS/Certificates#Certificates-IdentityProvider</a>
    </code>
  </li>
</ul>
<br/><br/>




<a id="oauth-2.0-client-assertion"></a>
## **OAuth 2.0 - Client Assertion**<br/>

All certificates under this category are Enterprise-issued certificates responsible for the creation and signing of client assertions.<br/><br/>

<a id="oauth-2.0-client-assertion:money-transfers-relationship"></a>
### **Money Transfers Relationship**

Description: These certificates support the Enterprise-to-Invest money transfers relationship.

Technical Owners:
  * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
  * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>

Required: No<br/>
Renewal:  No<br/>
Obseletion: Developers capable of importing certificates into the identity provider configuration store will not allow client assertion moving forward except for external clients. The existing relationship will be required to migrate to a shared secret to eliminate this unnecessary maintenance.

<a id="oauth-2.0-client-assertion:money-transfers-relationship:actions"></a>
Actions:
  1. (Enterprise; API Jacks) Import certificate into ??? store
  2. (Invest; Developers) Import certificate into the identity provider administrative tool for `ally-transfers-service` client identifier
  3. (Enterprise; API Jacks) Activate certificate<br/><br/>

Venafi:<br/>
  * Enterprise-owned: Yes
  * Users/Groups within Invest to Notify:
    * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
    * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>
    * `NAO\LZ6R96` - <a href="mailto:ramu.bandaru@invest.ally.com">ramu.bandaru@invest.ally.com</a>
    * `NAO\Invest DevOps` - <a href="mailto:InvestDevOps@invest.ally.com">InvestDevOps@invest.ally.com</a>
    * `NAO\Invest SRE Engineering` - <a href="mailto:SREEngineers@invest.ally.com">SREEngineers@invest.ally.com</a><br/><br/>

<a id="oauth-2.0-client-assertion:money-transfers-relationship:non-production"></a>
**Non-Production**<br/>
&emsp;Common Name: **transfers-svc-nonprod.int.ally.corp**<br/>
&emsp;Has Private Key: No<br/>
&emsp;Thumbprint: **F47A648FC94986E3779C3B21B580DE1395A555EB**<br/>
&emsp;Expiration: **Wednesday, October 16, 2024 3:25 PM -04:00**<br/><br/>

<a id="oauth-2.0-client-assertion:money-transfers-relationship:production"></a>
**Production**<br/>
&emsp;Common Name: **transfers-svc.int.ally.corp**<br/>
&emsp;Has Private Key: No<br/>
&emsp;Thumbprint: **7A9473FAEAF6B7859DB4E05E688ABD15403706ED**<br/>
&emsp;Expiration: **Friday, October 25, 2024 11:31 AM -04:00**<br/><br/><br/>




<a id="sso-saml-validation"></a>
## **SSO - SAML Validation**

All certificates under this category are Enterprise-issued certificates responsible for signing the SAML payloads sent out to the Invest service providers.

All certificates under this category typically expire at approximately the same datetime hence they are too be renewed at approximately the same datetime.

All certificates under this category are **reused** across applications, i.e., imports into Invest Octopus only need to be performed once and applied across all the below applications.

SSO-integrated enterprise environments that **require** consideration:

  * DEV
  * QA1
  * QA3
  * CAP
  * PROD A
  * PROD B
<br/><br/>


<a id="sso-saml-validation:developer-api"></a>
### **Developer API**

Description: The developer API is a publicly-facing, legacy service that provides an API for individuals and third-parties.<br/>

Technical Owners:
  * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
  * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>

Required: Yes<br/>
Renewal:  Yes<br/>
Obseletion: Please contact Invest technical leadership to ascertain whether or not these certificates are still required. These certificates will become obselete if 1. the customer-base is forced to migrate away from SSO to an alternative authentication process like Transmit or 2. the developer API is deprecated and sunset. <br/>

<a id="sso-saml-validation:developer-api:actions"></a>
Actions:
  1. (Enterprise; CIAM SRE) Import certificate into the Siteminder certificate stores and activate
  2. (Invest; DevOps) Import certificate into the Octopus certificate store
  3. (Invest; DevOps,Developers) Create a new release of the current production artifact and deploy. Annotate the release to inform of the certificate change (ex. suffix: -validation-certificate)<br/>

Venafi:<br/>
  * Enterprise-owned: Yes
  * Users/Groups within Invest to Notify:
    * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
    * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>
    * `NAO\LZ6R96` - <a href="mailto:ramu.bandaru@invest.ally.com">ramu.bandaru@invest.ally.com</a>
    * `NAO\Invest DevOps` - <a href="mailto:InvestDevOps@invest.ally.com">InvestDevOps@invest.ally.com</a>
    * `NAO\Invest SRE Engineering` - <a href="mailto:SREEngineers@invest.ally.com">SREEngineers@invest.ally.com</a><br/><br/>

<a id="sso-saml-validation:developer-api:non-production"></a>
**Non-Production**<br/>
&emsp;Common Name: **federation-ssl-nonprod.ally.corp**<br/>
&emsp;Has Private Key: No<br/>
&emsp;Thumbprint: **19874BBEFF64387971FD8EE18CA5B2D83C3986F4**<br/>
&emsp;Expiration: **Friday, February 24, 2023 8:22 PM -05:00**<br/><br/>

<a id="sso-saml-validation:developer-api:production"></a>
**Production**<br/>
&emsp;Common Name: **federation-ssl-prod.ally.corp**<br/>
&emsp;Has Private Key: No<br/>
&emsp;Thumbprint: **18854F47AC5C0171A09F7C60EC583F5F132C779E**<br/>
&emsp;Expiration: **Friday, March 24, 2023 11:41 AM -04:00**<br/><br/><br/>




<a id="sso-saml-validation:tapi"></a>
###  **TAPI**

Description: TAPI is a publicly-facing, legacy service that provides an API for the Android and iOS mobile applications.<br/>

Technical Owners:
  * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
  * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>

Required: Yes<br/>
Renewal:  Yes<br/>
Obseletion: Please contact Invest technical leadership to ascertain whether or not these certificates are still required. These certificates will become obselete if the mobile applications migrate away from SSO to an alternative authentication process like Transmit.<br/><br/>

<a id="sso-saml-validation:tapi:actions"></a>
Actions:
  1. (Enterprise; CIAM SRE) Import certificate into the Siteminder certificate stores and activate
  2. (Invest; DevOps) Import certificate into the Octopus certificate store
  3. (Invest; DevOps,Developers) Create a new release of the current production artifact and deploy. Annotate the release to inform of the certificate change (ex. suffix: -validation-certificate)<br/><br/>

Venafi:<br/>
  * Enterprise-owned: Yes
  * Users/Groups within Invest to Notify:
    * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
    * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>
    * `NAO\LZ6R96` - <a href="mailto:ramu.bandaru@invest.ally.com">ramu.bandaru@invest.ally.com</a>
    * `NAO\Invest DevOps` - <a href="mailto:InvestDevOps@invest.ally.com">InvestDevOps@invest.ally.com</a>
    * `NAO\Invest SRE Engineering` - <a href="mailto:SREEngineers@invest.ally.com">SREEngineers@invest.ally.com</a><br/><br/>

<a id="sso-saml-validation:non-production"></a>
**Non-Production**<br/>
&emsp;Common Name: **federation-ssl-nonprod.ally.corp**<br/>
&emsp;Has Private Key: No<br/>
&emsp;Thumbprint: **19874BBEFF64387971FD8EE18CA5B2D83C3986F4**<br/>
&emsp;Expiration: **Friday, February 24, 2023 8:22 PM -05:00**<br/><br/>

<a id="sso-saml-validation:tapi:production"></a>
**Production**<br/>
&emsp;Common Name: **federation-ssl-prod.ally.corp**<br/>
&emsp;Has Private Key: No<br/>
&emsp;Thumbprint: **18854F47AC5C0171A09F7C60EC583F5F132C779E**<br/>
&emsp;Expiration: **Friday, March 24, 2023 11:41 AM -04:00**<br/>
<br/><br/>




<a id="sso-saml-validation:live-service-provider"></a>
### **LIVE Service Provider**

Description: LIVE Service Provider is a publicly-facing, SSO-specific API for the LIVE application.

Technical Owners:
  * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
  * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>

Required: Yes<br/>
Renewal:  Yes<br/>
Obseletion: Please contact Invest technical leadership to ascertain whether or not these certificates are still required. These certificates will become obselete if the LIVE application migrates away from SSO to an alternative authentication process like Transmit. <br/><br/>

<a id="sso-saml-validation:live-service-provider:actions"></a>
Actions:
  1. (Enterprise; CIAM SRE) Import certificate into the Siteminder certificate stores and activate
  2. (Invest; DevOps) Import certificate into the Octopus certificate store
  3. (Invest; DevOps,Developers) Create a new release of the current production artifact and deploy. Annotate the release to inform of the certificate change (ex. suffix: -validation-certificate)<br/><br/>

Venafi:<br/>
  * Enterprise-owned: Yes
  * Users/Groups within Invest to Notify:
    * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
    * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>
    * `NAO\LZ6R96` - <a href="mailto:ramu.bandaru@invest.ally.com">ramu.bandaru@invest.ally.com</a>
    * `NAO\Invest DevOps` - <a href="mailto:InvestDevOps@invest.ally.com">InvestDevOps@invest.ally.com</a>
    * `NAO\Invest SRE Engineering` - <a href="mailto:SREEngineers@invest.ally.com">SREEngineers@invest.ally.com</a><br/><br/>

<a id="sso-saml-validation:live-service-provider:non-production"></a>
**Non-Production**<br/>
&emsp;Common Name: **federation-ssl-nonprod.ally.corp**<br/>
&emsp;Has Private Key: No<br/>
&emsp;Thumbprint: **19874BBEFF64387971FD8EE18CA5B2D83C3986F4**<br/>
&emsp;Expiration: **Friday, February 24, 2023 8:22 PM -05:00**<br/><br/>

<a id="sso-saml-validation:live-service-provider:production"></a>
**Production**<br/>
&emsp;Common Name: **federation-ssl-prod.ally.corp**<br/>
&emsp;Has Private Key: No<br/>
&emsp;Thumbprint: **18854F47AC5C0171A09F7C60EC583F5F132C779E**<br/>
&emsp;Expiration: **Friday, March 24, 2023 11:41 AM -04:00**<br/><br/><br/>




<a id="sso-saml-validation:ola-service-provider"></a>
### **OLA Service Provider**

Description: OLA Service Provider is a publicly-facing, SSO-specific API for the OLA application.

Technical Owners:
  * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
  * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>

Required: Yes<br/>
Renewal:  Yes<br/>
Obseletion: Please contact Invest technical leadership to ascertain whether or not these certificates are still required. These certificates will become obselete if the OLA application migrates away from SSO to an alternative authentication process like Transmit. <br/><br/>

<a id="sso-saml-validation:ola-service-provider:actions"></a>
Actions:
  1. (Enterprise; CIAM SRE) Import certificate into the Siteminder certificate stores and activate
  2. (Invest; DevOps) Import certificate into the Octopus certificate store
  3. (Invest; DevOps,Developers) Create a new release of the current production artifact and deploy. Annotate the release to inform of the certificate change (ex. suffix: -validation-certificate)<br/><br/>

Venafi:<br/>
  * Enterprise-owned: Yes
  * Users/Groups within Invest to Notify:
    * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
    * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>
    * `NAO\LZ6R96` - <a href="mailto:ramu.bandaru@invest.ally.com">ramu.bandaru@invest.ally.com</a>
    * `NAO\Invest DevOps` - <a href="mailto:InvestDevOps@invest.ally.com">InvestDevOps@invest.ally.com</a>
    * `NAO\Invest SRE Engineering` - <a href="mailto:SREEngineers@invest.ally.com">SREEngineers@invest.ally.com</a><br/><br/>

<a id="sso-saml-validation:ola-service-provider:non-production"></a>
**Non-Production**<br/>
&emsp;Common Name: **federation-ssl-nonprod.ally.corp**<br/>
&emsp;Has Private Key: No<br/>
&emsp;Thumbprint: **19874BBEFF64387971FD8EE18CA5B2D83C3986F4**<br/>
&emsp;Expiration: **Friday, February 24, 2023 8:22 PM -05:00**<br/><br/>

<a id="sso-saml-validation:ola-service-provider:production"></a>
**Production**<br/>
&emsp;Common Name: **federation-ssl-prod.ally.corp**<br/>
&emsp;Has Private Key: No<br/>
&emsp;Thumbprint: **18854F47AC5C0171A09F7C60EC583F5F132C779E**<br/>
&emsp;Expiration: **Friday, March 24, 2023 11:41 AM -04:00**<br/><br/><br/>




<a id="sso-saml-signing"></a>
## **SSO - SAML Signing**

All certificates under this category are Invest-issued certificates responsible for signing the SAML payloads sent out to the Enterprise CA Federation identity manager.<br/>

All certificates under this category typically expire at approximately the same datetime hence they are too be renewed at approximately the same datetime.<br/>

SSO-integrated enterprise environments that **require** consideration:

  * DEV
  * QA1
  * QA3
  * CAP
  * PROD A
  * PROD B
<br/><br/>


<a id="sso-saml-signing:developer-api"></a>
### **Developer API**

Description: The developer API is a publicly-facing, legacy service that provides an API for individuals and third-parties.

Technical Owners:
  * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
  * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>

Required: Yes<br/>
Renewal:  Yes<br/>
Obseletion: Please contact Invest technical leadership to ascertain whether or not these certificates are still required. These certificates will become obselete if 1. the customer-base is forced to migrate away from SSO to an alternative authentication process like Transmit or 1. the developer API is deprecated and sunset. <br/><br/>

<a id="sso-saml-signing:developer-api:actions"></a>
Actions:
  1. (Invest; DevOps) Import certificate into the Octopus certificate store
  2. (Invest; DevOps,Developers) Create a new release of the current production artifact and deploy. Annotate the release to inform of the certificate change (ex. suffix: -signing-certificate)
  3. (Enterprise; CIAM SRE) Import certificate into the Siteminder certificate stores and activate<br/><br/>

Venafi:<br/>
  * Users/Groups to Notify:
    * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
    * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>
    * `NAO\LZ6R96` - <a href="mailto:ramu.bandaru@invest.ally.com">ramu.bandaru@invest.ally.com</a>
    * `NAO\Invest DevOps` - <a href="mailto:InvestDevOps@invest.ally.com">InvestDevOps@invest.ally.com</a>
    * `NAO\Invest SRE Engineering` - <a href="mailto:SREEngineers@invest.ally.com">SREEngineers@invest.ally.com</a>
    * `NAO\Agile Team - Guardians` - <a href="AgileTeam-Guardians@ally.com">AgileTeam-Guardians@ally.com</a>
    * `NAO\Ally CIAM SRE` - <a href="allyciamsre@ally.com">allyciamsre@ally.com</a><br/><br/>

<a id="sso-saml-signing:developer-api:non-production"></a>
**Non-Production**
<ul style="list-style: none; margin-top: -8px; padding-left: 20px;">
  <li>Common Name: <strong>developers-nonprod.tradeking.com</strong></li>
  <li>Has Private Key: Yes<br/>
  <li>Thumbprint: <strong>75AEB2742C4438F1E6E0228F5AF54BA473E99D6C</strong></li>
  <li>Expiration: <strong>Sunday, August 13, 2023 11:49 AM -04:00</strong></li>
  <li>Venafi Note:<br/><br/>
    <code>
      Non-Production - This certificate supports the SSO flow for bank-to-invest SAML rainbow interactions for the developer API product. Renewal requires 1. an Octopus certificate store import, 2. installation on servers via Octopus deployments (project: apiv2-developers) with updated variables, and 3. Siteminder (owned by: CIAM SRE) certificate store imports for ALL lower environments. Invest Sustain manages this cert. For more details, consult <a href="https://confluence.int.ally.com/display/AIS/Certificates#Certificates-DeveloperAPI.1">https://confluence.int.ally.com/display/AIS/Certificates#Certificates-DeveloperAPI.1</a>
    </code>
  </li>
</ul><br/><br/>

<a id="sso-saml-signing:developer-api:production"></a>
**Production**
<ul style="list-style: none; margin-top: -8px; padding-left: 20px;">
  <li>Common Name: <strong>developers.tradeking.com</strong></li>
  <li>Has Private Key: Yes</li>
  <li>Thumbprint: <strong>50D6CC32736FCD5DAC16B301E66A9CD773E60F4B</strong></li>
  <li>Expiration: <strong>Sunday, August 13, 2023 11:48 AM -04:00</strong></li>
  <li>Venafi Note:<br/><br/>
    <code>
      Production - This certificate supports the SSO flow for bank-to-invest SAML rainbow interactions for the developer API product. Renewal requires 1. an Octopus certificate store import, 2. installation on servers via Octopus deployments (project: apiv2-developers) with updated variables, and 3. Siteminder (owned by: CIAM SRE) certificate store imports on BOTH prod A and prod B. Invest Sustain manages this cert. For more details, consult <a href="https://confluence.int.ally.com/display/AIS/Certificates#Certificates-DeveloperAPI.1">https://confluence.int.ally.com/display/AIS/Certificates#Certificates-DeveloperAPI.1</a>
    </code>
  </li>
</ul><br/><br/>




<a id="sso-saml-signing:tapi"></a>
### **TAPI**

Description: TAPI is a publicly-facing, legacy service that provides an API for the Android and iOS mobile applications.

Technical Owners:
  * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
  * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>

Required: Yes<br/>
Renewal:  Yes<br/>
Obseletion: Please contact Invest technical leadership to ascertain whether or not these certificates are still required. These certificates will become obselete if the mobile applications migrate away from SSO to an alternative authentication process like Transmit.<br/><br/>

<a id="sso-saml-signing:tapi:actions"></a>
Actions:
  1. (Invest; DevOps) Import certificate into the Octopus certificate store
  2. (Invest; DevOps,Developers) Create a new release of the current production artifact and deploy. Annotate the release to inform of the certificate change (ex. suffix: -signing-certificate)
  3. (Enterprise; CIAM SRE) Import certificate into the Siteminder certificate stores and activate<br/><br/>

Venafi:<br/>
  * Users/Groups to Notify:
    * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
    * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>
    * `NAO\LZ6R96` - <a href="mailto:ramu.bandaru@invest.ally.com">ramu.bandaru@invest.ally.com</a>
    * `NAO\Invest DevOps` - <a href="mailto:InvestDevOps@invest.ally.com">InvestDevOps@invest.ally.com</a>
    * `NAO\Invest SRE Engineering` - <a href="mailto:SREEngineers@invest.ally.com">SREEngineers@invest.ally.com</a>
    * `NAO\Agile Team - Guardians` - <a href="AgileTeam-Guardians@ally.com">AgileTeam-Guardians@ally.com</a>
    * `NAO\Ally CIAM SRE` - <a href="allyciamsre@ally.com">allyciamsre@ally.com</a><br/><br/>

<a id="sso-saml-signing:tapi:non-production"></a>
**Non-Production**
<ul style="list-style: none; margin-top: -8px; padding-left: 20px;">
  <li>Common Name: <strong>tapi-nonprod.tradeking.com</strong></li>
  <li>Has Private Key: Yes</li>
  <li>Thumbprint: <strong>D9C43CC328DBAF7046F8AEC0EC068598D8911C38</strong></li>
  <li>Expiration: <strong>Saturday, August 12, 2023 1:29 PM -04:00</strong></li>
  <li>Venafi Note:</br><br/>
    <code>
      Non-Production - This certificate supports the SSO flow for bank-to-invest SAML rainbow interactions for the mobile product. Renewal requires 1. an Octopus certificate store import, 2. installation on servers via Octopus deployments (project: tapi) with updated variables, and 3. Siteminder (owned by: CIAM SRE) certificate store imports for ALL lower environments. Invest Sustain manages this cert. For more details, consult <a href="https://confluence.int.ally.com/display/AIS/Certificates#Certificates-TAPI.1">https://confluence.int.ally.com/display/AIS/Certificates#Certificates-TAPI.1</a>
    </code>
  </li>
</ul><br/><br/>

<a id="sso-saml-signing:tapi:production"></a>
**Production**
<ul style="list-style: none; margin-top: -8px; padding-left: 20px;">
  <li>Common Name: <strong>tapi.tradeking.com</strong></li>
  <li>Has Private Key: Yes</li>
  <li>Thumbprint: <strong>3CB38E124F58D4AFACFAA53E7BB0AB04EF2930EB</strong></li>
  <li>Expiration: <strong>Saturday, August 12, 2023 1:31 PM -04:00</strong></li>
  <li>Venafi Note:<br/><br/>
    <code>
      Production - This certificate supports the SSO flow for bank-to-invest SAML rainbow interactions for the mobile product. Renewal requires 1. an Octopus certificate store import, 2. installation on servers via Octopus deployments (project: tapi) with updated variables, and 3. Siteminder (owned by: CIAM SRE) certificate store imports on BOTH prod A and prod B. Invest Sustain manages this cert. For more details, consult <a href="https://confluence.int.ally.com/display/AIS/Certificates#Certificates-TAPI.1">https://confluence.int.ally.com/display/AIS/Certificates#Certificates-TAPI.1</a>
    </code>
  </li>
</ul><br/><br/>




<a id="sso-saml-signing:live-service-provider"></a>
### **LIVE Service Provider**

Description: LIVE Service Provider is a publicly-facing, SSO-specific API for the LIVE application.

Technical Owners:
  * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
  * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>

Required: Yes<br/>
Renewal:  Yes<br/>
Obseletion: Please contact Invest technical leadership to ascertain whether or not these certificates are still required. These certificates will become obselete if the LIVE application migrates away from SSO to an alternative authentication process like Transmit. <br/><br/>

<a id="sso-saml-signing:live-service-provider:actions"></a>
Actions:
  1. (Invest; DevOps) Import certificate into the Octopus certificate store
  2. (Invest; DevOps,Developers) Create a new release of the current production artifact and deploy. Annotate the release to inform of the certificate change (ex. suffix: -signing-certificate)
  3. (Enterprise; CIAM SRE) Import certificate into the Siteminder certificate stores and activate<br/><br/>

Venafi:<br/>
  * Users/Groups to Notify:
    * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
    * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>
    * `NAO\LZ6R96` - <a href="mailto:ramu.bandaru@invest.ally.com">ramu.bandaru@invest.ally.com</a>
    * `NAO\Invest DevOps` - <a href="mailto:InvestDevOps@invest.ally.com">InvestDevOps@invest.ally.com</a>
    * `NAO\Invest SRE Engineering` - <a href="mailto:SREEngineers@invest.ally.com">SREEngineers@invest.ally.com</a>
    * `NAO\Agile Team - Guardians` - <a href="AgileTeam-Guardians@ally.com">AgileTeam-Guardians@ally.com</a>
    * `NAO\Ally CIAM SRE` - <a href="allyciamsre@ally.com">allyciamsre@ally.com</a><br/><br/>

<a id="sso-saml-signing:live-service-provider:non-production"></a>
**Non-Production**
<ul style="list-style: none; margin-top: -8px; padding-left: 20px;">
  <li>Common Name: <strong>live.invest-nonprod.ally.com</strong></li>
  <li>Has Private Key: Yes</li>
  <li>Thumbprint: <strong>E07770DD6D87A37438867DEA778AE6CBA11670C4</strong></li>
  <li>Expiration: <strong>Saturday, August 12, 2023 1:28 PM -04:00</strong></li>
  <li>Venafi Note:<br/><br/>
    <code>
      Non-Production - This certificate supports the SSO flow for bank-to-invest SAML rainbow interactions for the LIVE product. Renewal requires 1. an Octopus certificate store import, 2. installation on servers via Octopus deployments (project: phoenix-sso-service-provider) with updated variables, and 3. Siteminder (owned by: CIAM SRE) certificate store imports for ALL lower environments. Invest Sustain manages this cert. For more details, consult <a href="https://confluence.int.ally.com/display/AIS/Certificates#Certificates-LIVEServiceProvider.1">https://confluence.int.ally.com/display/AIS/Certificates#Certificates-LIVEServiceProvider.1</a>
    </code>
  </li>
</ul><br/><br/>

<a id="sso-saml-signing:live-service-provider:production"></a>
**Production**
<ul style="list-style: none; margin-top: -8px; padding-left: 20px;">
  <li>Common Name: <strong>live.invest.ally.com</strong></li>
  <li>Has Private Key: Yes</li>
  <li>Thumbprint: <strong>2903D1D61B0FB07125C8D01D9905AD4342C29E0E</strong></li>
  <li>Expiration: <strong>Saturday, August 12, 2023 1:30 PM -04:00</strong></li>
  <li>Venafi Note:<br/><br/>
    <code>
      Production - This certificate supports the SSO flow for bank-to-invest SAML rainbow interactions for the LIVE product. Renewal requires 1. an Octopus certificate store import, 2. installation on servers via Octopus deployments (project: phoenix-sso-service-provider) with updated variables, and 3. Siteminder (owned by: CIAM SRE) certificate store imports on BOTH prod A and prod B. Invest Sustain manages this cert. For more details, consult <a href="https://confluence.int.ally.com/display/AIS/Certificates#Certificates-LIVEServiceProvider.1">https://confluence.int.ally.com/display/AIS/Certificates#Certificates-LIVEServiceProvider.1</a>
    </code>
  </li>
</ul><br/><br/>




<a id="sso-saml-signing:ola-service-provider"></a>
### **OLA Service Provider**<br/>

Description: OLA Service Provider is a publicly-facing, SSO-specific API for the OLA application.

Technical Owners:
  * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
  * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>

Required: Yes<br/>
Renewal:  Yes<br/>
Obseletion: Please contact Invest technical leadership to ascertain whether or not these certificates are still required. These certificates will become obselete if the OLA application migrates away from SSO to an alternative authentication process like Transmit. <br/><br/>

<a id="sso-saml-signing:ola-service-provider:actions"></a>
Actions:
  1. (Invest; DevOps) Import certificate into the Octopus certificate store
  2. (Invest; DevOps,Developers) Create a new release of the current production artifact and deploy. Annotate the release to inform of the certificate change (ex. suffix: -signing-certificate)
  3. (Enterprise; CIAM SRE) Import certificate into the Siteminder certificate stores and activate<br/><br/>

Venafi:<br/>
  * Users/Groups to Notify:
    * `NAO\MZDK9R` - <a href="mailto:sheldon.smith@invest.ally.com">sheldon.smith@invest.ally.com</a>
    * `NAO\QZ2L4Y` - <a href="mailto:craig.stephans@invest.ally.com">craig.stephans@invest.ally.com</a>
    * `NAO\LZ6R96` - <a href="mailto:ramu.bandaru@invest.ally.com">ramu.bandaru@invest.ally.com</a>
    * `NAO\Invest DevOps` - <a href="mailto:InvestDevOps@invest.ally.com">InvestDevOps@invest.ally.com</a>
    * `NAO\Invest SRE Engineering` - <a href="mailto:SREEngineers@invest.ally.com">SREEngineers@invest.ally.com</a>
    * `NAO\Agile Team - Guardians` - <a href="AgileTeam-Guardians@ally.com">AgileTeam-Guardians@ally.com</a>
    * `NAO\Ally CIAM SRE` - <a href="allyciamsre@ally.com">allyciamsre@ally.com</a><br/><br/>

<a id="sso-saml-signing:ola-service-provider:non-production"></a>
**Non-Production**
<ul style="list-style: none; margin-top: -8px; padding-left: 20px;">
  <li>Common Name: <strong>invest.int.ally.com</strong></li>
  <li>Has Private Key: Yes</li>
  <li>Thumbprint: <strong>CDAC204E16410DDBC788F714392520488131B9BF</strong></li>
  <li>Expiration: <strong>Sunday, August 13, 2023 11:55 AM -04:00</strong></li>
  <li>Venafi Note:<br/><br/>
    <code>
      Non-Production - This certificate supports the SSO flow for bank-to-invest SAML rainbow interactions for the OLA product. Renewal requires 1. an Octopus certificate store import, 2. installation on servers via Octopus deployments (project: ola-sso-service-provider) with updated variables, and 3. Siteminder (owned by: CIAM SRE) certificate store imports for ALL lower environments. Invest Sustain manages this cert. For more details, consult <a href="https://confluence.int.ally.com/display/AIS/Certificates#Certificates-OLAServiceProvider.1">https://confluence.int.ally.com/display/AIS/Certificates#Certificates-OLAServiceProvider.1</a>
    </code>
  </li>
</ul>
<br/><br/>

<a id="sso-saml-signing:ola-service-provider:production"></a>
**Production**
<ul style="list-style: none; margin-top: -8px; padding-left: 20px;">
  <li>Common Name: <strong>invest.ally.com</strong></li>
  <li>Has Private Key: Yes</li>
  <li>Thumbprint: <strong>8879AD5F3D3CC0AF38BE82A07567AAC0D4F339B8</strong></li>
  <li>Expiration: <strong>Sunday, August 13, 2023 11:52 AM -04:00</strong></li>
  <li>Venafi Note:<br/><br/>
    <code>
      Production - This certificate supports the SSO flow for bank-to-invest SAML rainbow interactions for the OLA product. Renewal requires 1. an Octopus certificate store import, 2. installation on servers via Octopus deployments (project: ola-sso-service-provider) with updated variables, and 3. Siteminder (owned by: CIAM SRE) certificate store imports on BOTH prod A and prod B. Invest Sustain manages this cert. For more details, consult <a href="https://confluence.int.ally.com/display/AIS/Certificates#Certificates-OLAServiceProvider.1">https://confluence.int.ally.com/display/AIS/Certificates#Certificates-OLAServiceProvider.1</a>
    </code>
  </li>
</ul>
<br/><br/>




<a id="scheduled-jobs"></a>
## **Scheduled Jobs**

All certificates under this category are Invest-issued certificates required for various scheduled jobs.<br/><br/>

<a id="scheduled-jobs:hangfire"></a>
### **Hangfire**

Description: Hangfire is private job scheduler. The jobs running in Hangfire are specific to managed portfolio.<br/>

Technical Owners:
  * ???
  * `NAO\LZ6R96` - <a href="mailto:ramu.bandaru@invest.ally.com">ramu.bandaru@invest.ally.com</a>

Required: Yes<br/>
Renewal:  Yes<br/>
Obseletion: Please contact Invest technical leadership to ascertain whether or not these certificates are still required. These certificates will become obselete if 1. the Hangfire jobs are migrated or 2. an alternative method to securely access service accounts is taken.<br/><br/>

<a id="scheduled-jobs:hangfire:actions"></a>
Actions:
  1. (Invest; DevOps) Import certificate into the Octopus certificate store
  2. (Invest; DevOps,Developers) ?????<br/><br/>

Venafi:<br/>
  * Users/Groups to Notify:
    * `NAO\LZ6R96` - <a href="mailto:ramu.bandaru@invest.ally.com">ramu.bandaru@invest.ally.com</a>
    * `NAO\Invest DevOps` - <a href="mailto:InvestDevOps@invest.ally.com">InvestDevOps@invest.ally.com</a>
    * `NAO\Invest SRE Engineering` - <a href="mailto:SREEngineers@invest.ally.com">SREEngineers@invest.ally.com</a><br/><br/>

<a id="scheduled-jobs:hangfire:development"></a>
**Development**
<ul style="list-style: none; margin-top: -8px; padding-left: 20px;">
  <li>Common Name: <strong>hangfire.dev.tk.local</strong></li>
  <li>Has Private Key: Yes</li>
  <li>Thumbprint: <strong>EB5791F4C9DE9F00965913FB4286FB16BB64F41F</strong></li>
  <li>Expiration: <strong>Thursday, December 8, 2022 2:26 PM -05:00</strong></li>
  <li>Venafi Note:<br/><br/>
    <code>
      Development - This certificate supports the Hangfire job scheduler application. Renewal requires 1. an Octopus certificate store import and 2. ?????. Invest Sustain manages this cert. For more details, consult <a href="https://confluence.int.ally.com/display/AIS/Certificates#Certificates-Hangfire">https://confluence.int.ally.com/display/AIS/Certificates#Certificates-Hangfire</a>
    </code>
  </li>
</ul><br/><br/>

<a id="scheduled-jobs:hangfire:qa"></a>
**QA**
<ul style="list-style: none; margin-top: -8px; padding-left: 20px;">
  <li>Common Name: <strong>hangfire.qa.tk.local</strong></Li>
  <li>Has Private Key: Yes</li>
  <li>Thumbprint: <strong>BB4A5744C781C8F92083500C1A74F9AF2FFE8C86</strong></li>
  <li>Expiration: <strong>Thursday, December 8, 2022 2:28 PM -05:00</strong></li>
  <li>Venafi Note:<br/><br/>
    <code>
      Development - This certificate supports the Hangfire job scheduler application. Renewal requires 1. an Octopus certificate store import and 2. ?????. Invest Sustain manages this cert. For more details, consult <a href="https://confluence.int.ally.com/display/AIS/Certificates#Certificates-Hangfire">https://confluence.int.ally.com/display/AIS/Certificates#Certificates-Hangfire</a>
    </code>
  </li>
</ul><br/><br/>

<a id="scheduled-jobs:hangfire:production"></a>
**Production**
<ul style="list-style: none; margin-top: -8px; padding-left: 20px;">
  <li>Common Name: <strong>hangfire.prod.tk.local</strong></li>
  <li>Has Private Key: Yes</li>
  <li>Thumbprint: <strong>A9E24D3D82388D1C0617393B9CAA2864E2BED568</strong></li>
  <li>Expiration: <strong>Thursday, December 8, 2022 2:31 PM -05:00</strong></li>
  <li>Venafi Note:<br/><br/>
    <code>
      Development - This certificate supports the Hangfire job scheduler application. Renewal requires 1. an Octopus certificate store import and 2. ?????. Invest Sustain manages this cert. For more details, consult <a href="https://confluence.int.ally.com/display/AIS/Certificates#Certificates-Hangfire">https://confluence.int.ally.com/display/AIS/Certificates#Certificates-Hangfire</a>
    </code>
  </li>
</ul><br/><br/>




<a id="vendors"></a>
## **Vendors**

All certificates under this category are vendor-issued certificates required for various reasons.<br/><br/>

<a id="vendors:nasdaq-inc"></a>
### **Nasdaq Inc**

<a id="vendors:nasdaq-inc:nasdaq-fund-network-feed"></a>
#### **Nasdaq Fund Network Feed**

Description: The <a href="https://nfn.nasdaq.com/Default.aspx">NFN</a> system facilitates the collection and dissemination of valuation data for a multitude of asset classes.

Technical Owners:
  * `NAO\Tickerplant` - <a href="mailto:tickerplant@invest.ally.com">tickerplant@invest.ally.com</a>

Required: Yes<br/>
Renewal: Automatic<br/>

<a id="vendors:nasdaq-inc:nasdaq-fund-network-feed:actions"></a>
Actions: N/A (SDK is responsible for retrieval and store installation)

<a id="vendors:nasdaq-inc:nasdaq-fund-network-feed:trusted-store"></a>
**Trusted Store**
<ul style="list-style: none; margin-top: -8px; padding-left: 20px;">
  <li>Common Name: <strong>keycloak.pro.us-east-1.ncpgismdic.nadq.pub</strong></li>
  <li>Has Private Key: Yes</li>
</ul><br/>

<a id="vendors:nasdaq-inc:nasdaq-fund-network-feed:kafka"></a>
**Broker**
<ul style="list-style: none; margin-top: -8px; padding-left: 20px;">
  <li>Common Name: <strong>clouddataservice.broker.bootstrap.nasdaq.com</strong></li>
  <li>Has Private Key: No</li>
</ul><br/><br/>